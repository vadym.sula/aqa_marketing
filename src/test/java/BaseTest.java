import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import utils.BrowserType;
import utils.WebDriverFactory;

public class BaseTest {
    protected WebDriver driver = null;

    protected void goToUrl(String url) {
        driver.get(url);
    }

    @BeforeSuite
    public void InitAll() {
        driver = WebDriverFactory.initDriver(BrowserType.CHROME);
        driver.manage().window().maximize();
    }

    @AfterSuite
    public void deactivateAll() {
        if (driver != null) {
            driver.quit();
        }
    }
}
