package utils;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class ParserCsv {
    public Set<String[]> ParseCsvToSet() throws IOException, CsvException {
        CSVReader reader = new CSVReader(new FileReader("src/main/resources/JobSkills.csv"));
        List<String[]> temporaryList = reader.readAll();
        var keyWords = Set.copyOf(temporaryList);
        return keyWords;
    }
}

