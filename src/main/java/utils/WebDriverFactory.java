package utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriverFactory {
    public static WebDriver initDriver(BrowserType browserType) {
        WebDriverManager.chromedriver().setup();
        return new ChromeDriver();
    }

    public static WebDriver initDriver() {
        String browserType = System.getProperty("driverType", "chrome");
        try {
            BrowserType webDriverType = BrowserType.valueOf(browserType.toUpperCase());
            return initDriver(webDriverType);
        } catch (Exception e){
            System.out.println("This driver is not supported yet!!");
            System.exit(-1);
        }

        return null;
    }
}
